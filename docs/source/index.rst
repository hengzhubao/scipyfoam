.. sciPyFoam documentation master file, created by
   sphinx-quickstart on Sat Apr 25 15:47:31 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to sciPyFoam's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   sciPyFoam/modules.rst
   sciPyFoam/postProcessing/modules.rst

   


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
